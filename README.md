<h3 align="center">Database</h3>


### Installation
1. Create the Database
   ```sh
   CREATE DATABASE IF NOT EXISTS davesportfolio;
   ```
2. Create tables and insert default data
```sh
Run davesportfoliofulldump.sql  as a query against the davesportfolio Database that was created in step 1
```

3. Ensure you have a user created called portfolioDB with the password 12345678
```sh
USE mysql;
CREATE USER 'portfolioDB'@'localhost' IDENTIFIED WITH mysql_native_password BY '12345678';
GRANT ALL PRIVILEGES ON *.* TO 'portfolioDB'@'localhost';
flush privileges;
```